import MyModal from '../../components/Modal'
import ProductDetail from '../../components/ProductDetails'
import ModalContextProvider from '../../context/ModalContext'
import {useRouter} from 'next/router'
import { useContext, useEffect, useState } from 'react'
import { AppContext } from '../../context/AppContext'


const Product = (props) => {
    const { state } = useContext(AppContext)
    const router = useRouter()
    const [ product, setProduct ] = useState({})
    
    useEffect(() => {
        state.products.forEach(product => {
            if (product.id === router.query.id) {
                setProduct(product)
            }
        })
    }, [router.query.id, state])

    return (
        <>  
            <ModalContextProvider>
                <ProductDetail product={product} />
                <MyModal />
            </ModalContextProvider>
        </>
    )
}

export default Product

export const getStaticProps = async () => {
    return {
        props: {}
    }
}

export const getStaticPaths = async () => {
    const paths = await getProdId()

    return {
        paths: paths,
        fallback: false
    }
}
export const getProdId = async () => {
    const productIds = await new Promise((resolve, reject) => {
        resolve(['1', '2', '3', '4', '5'])
    })

    return productIds.map(prod => ({
        params: {
            id: `${prod}`
        }
    }))
}