import { Button, Container, Grid, Paper, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import React, { useContext } from 'react'
import Image from 'next/image'
import Head from 'next/head'
import { Wrapper } from './styled-component/Wrapper'
import { useStylesButton } from './common/button'
import { ModalContext } from '../context/ModalContext'
import useViewMore from '../hooks/useViewMore'
const useStyles = makeStyles({
    roundedImg: {
        borderRadius: 12
    }
})
const ProductDetail = ({ product }) => {
    const { dispatchModalState } = useContext(ModalContext)
    const { Description } = useViewMore()
    const classes = useStyles()
    const btnClasses = useStylesButton()

    const handleOpen = () => {
        dispatchModalState({ isOpen: true, product: { ...product } })
    }
    return (
        <>
            <Head>
                <title>{product.name}</title>
            </Head>
            <Container>
                <Paper sx={{ borderRadius: 4 }}>
                    <Wrapper>
                        <Image className={classes.roundedImg} width={500}
                            height={500} alt='' src={product.img || '/images/jordan-1.jpg'} />
                        <Grid container spacing={2} sx={{ flexDirection: 'column', justifyContent: 'space-between', paddingX: 4, paddingTop: 2 }}>
                            <Grid container spacing={2} sx={{ flexDirection: 'column', paddingTop: 2 }}>
                                <Grid item>
                                    <Typography variant='h3'>
                                        {product.name || 'Nike Jordan 1'}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant='h4'>
                                        Price: <Typography variant='span' sx={{ color: 'red' }}>{product.price || 340} $</Typography>
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Description content={product.description} />
                                </Grid>
                            </Grid>
                            <Grid container sx={{ paddingBottom: 2, justifyContent: 'flex-end' }}>
                                <Grid item>
                                    <Button onClick={handleOpen} className={btnClasses.btn} variant="outlined">Edit</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Wrapper>
                </Paper>
            </Container>
        </>

    )
}

export default ProductDetail