import { Alert, Button, Dialog, DialogActions, DialogContent, DialogTitle, Snackbar, TextField } from '@mui/material'
import React, { forwardRef, useContext, useState } from 'react'
import { useForm } from 'react-hook-form'
import { AppContext } from '../context/AppContext'
import { ModalContext } from '../context/ModalContext'
import { useStylesButton } from './common/button'

const SnackbarAlert = forwardRef(
    function SnackbarAlert(props, ref) {
        return <Alert elevation={6} ref={ref} {...props} />
    }
)

const MyModal = () => {
    const { modalState, dispatchModalState } = useContext(ModalContext)
    const { state, dispatch } = useContext(AppContext)
    const [isSnackOpen, setSnackOpen] = useState(false)
    const { register, handleSubmit } = useForm()
    const btnClasses = useStylesButton()
    const handleDisagree = () => {
        dispatchModalState({ isOpen: false })
    }
    const handleCloseSnack = () => {
        setSnackOpen(false)
    }
    const onSubmit = (data) => {
        dispatchModalState({ isOpen: false, product: { ...data } })
        const arr = state.products.map(prev => {
            if (prev.id === modalState.product.id) {
                return { ...prev, ...data }
            }
            return prev
        })
        dispatch({ products: [...arr] })
        setSnackOpen(true)
    }

    return (
        <>
            {/* <Button onClick={handleOpen} className={btnClasses.btn} variant="outlined">Edit</Button> */}
            <Dialog
                open={modalState.isOpen}
                onClose={handleDisagree}
            >
                <DialogTitle >
                    Edit Product
                </DialogTitle>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogContent dividers sx={{ minWidth: 400, paddingBottom: 4 }}>
                        <TextField
                            sx={{ paddingBottom: 2 }}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name"
                            type="name"
                            fullWidth
                            variant="standard"
                            defaultValue={modalState.product.name}
                            {...register("name")}
                        />
                        <TextField
                            sx={{ paddingBottom: 2 }}
                            autoFocus
                            margin="dense"
                            id="price"
                            label="Price"
                            type="number"
                            fullWidth
                            variant="standard"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            defaultValue={modalState.product.price}
                            {...register("price")}
                        />
                        <TextField
                            sx={{ paddingBottom: 2 }}
                            autoFocus
                            margin="dense"
                            id="description"
                            label="Description"
                            type="description"
                            fullWidth
                            variant="standard"
                            multiline={true}
                            defaultValue={modalState.product.description}
                            {...register("description")
                        }
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button className={btnClasses.btnCancel} onClick={handleDisagree}>Cancel</Button>
                        <Button className={btnClasses.btnSubmit} type="submit" autoFocus>
                            Submit
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
            <Snackbar
                autoHideDuration={3000}
                open={isSnackOpen}
                onClose={handleCloseSnack}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
            >
                <SnackbarAlert onClose={handleCloseSnack} severity='success' color='success'>
                    Edit Success !!!
                </SnackbarAlert>
            </Snackbar>
        </>
    )
}

export default MyModal