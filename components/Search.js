import { Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@mui/material'
import { BiSearchAlt2 } from 'react-icons/bi'
import React, { useEffect } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useRouter } from 'next/router';

const categories = [{
    id: '1',
    value: 'all',
    name: 'All'
}, {
    id: '2',
    value: 'sneacky',
    name: 'Sneacky'
}, {
    id: '3',
    value: 'sport',
    name: 'Sport'
}]
const Search = () => {
    const router = useRouter()
    const { register, handleSubmit, setValue, control,reset } = useForm()
    const onSubmit = (data) => {
        router.push({
            pathname: '/products',
            query: {
                ...data
            }
        })
    };

    useEffect(() => {
        reset({
            search: router.query.search || '',
            category: router.query.category || 'all'
        })
    },[router.query])
    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Grid container sx={{ marginBottom: 2, paddingX: 4 }}>
                    <Grid container sx={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: 'white', borderRadius: 2 }}>
                        <Grid item sx={{ marginBottom: 0.5 }}>
                            <TextField
                                sx={{ backgroundColor: 'white' }}
                                autoFocus
                                margin="dense"
                                id="search"
                                label="Search"
                                type="text"
                                fullWidth
                                defaultValue={router.query.search || ''}
                                variant="outlined"
                                size="small"
                                {...setValue('search', router.query?.search)}
                                {...register("search")}
                            />
                        </Grid>
                        <Grid item>
                            <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
                                <InputLabel id="category">Category</InputLabel>
                                <Controller
                                    name="category"
                                    control={control}
                                    render={({ field: {
                                        onChange,
                                        value,
                                        name
                                    } }) => {
                                        console.log(value)
                                        return (
                                            <Select
                                                labelId="category"
                                                label="category"
                                                name={name}
                                                value={value || 'all'}
                                                onChange={(e) => {
                                                    return onChange(e)
                                                }}
                                                defaultValue={router.query.category || 'all'}
                                            >
                                                {categories.map((category) => {
                                                    return <MenuItem key={category.id} value={category.value}>{category.name}</MenuItem>
                                                })}
    
                                            </Select>
                                        )}
                                    }
                                />
                            </FormControl>
                        </Grid>
                        <Grid item sx={{ display: 'flex', height: '42px', alignItems: 'flex-end', marginBottom: 0.5 }}>
                            <Button
                                aria-label="search"
                                type="submit"
                                sx={{
                                    backgroundColor: "white",
                                    color: "black",
                                    height: "40px", width: 20,
                                    '&:hover': {
                                        backgroundColor: '#ddd'
                                    }
                                }}
                            >
                                <BiSearchAlt2 size={20} />
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        </>
    )
}

export default Search