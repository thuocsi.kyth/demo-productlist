import { AppBar, Box, Container, Toolbar, Typography, Button } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { AiOutlineShoppingCart } from 'react-icons/ai'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'

const pages = [
    { id: "1", name: "Home", href: "/" },
    { id: "2", name: "Products", href: "/products" },
    { id: "3", name: "About", href: "/about" },
]
export const useStyles = makeStyles({
    btn: {
        '&:hover': {
            background: "#bbbbbb"
        },
        background: '#fff',
        color: 'black',
        outline: 'black'
    },
    link: {
        '&:hover': {
            color: '#bbbbbb'
        }
    }
})
const NavBar = () => {
    const classes = useStyles()
    const router = useRouter()
    return (
        <AppBar position="static">
            <Container maxWidth="xl" sx={{ backgroundColor: 'black' }}>
                <Toolbar sx={{ justifyContent: 'space-between' }}>
                    <Typography variant="h4">
                        <Link href="/">
                            Nikey
                        </Link>
                    </Typography>
                    <Box sx={{
                        display: 'flex',
                        color: 'white',
                        justifyContent: 'space-evenly',
                        width: '400px'
                    }}
                    >
                        {pages.map(page => <Link key={page.id} href={page.href}>
                            <a className={classes.link}> {page.name}</a>
                        </Link>)}
                    </Box>
                    <Button variant='contained' onClick={() => {
                        router.push({pathname:'/products',query: null})
                    }} className={classes.btn}>
                        <AiOutlineShoppingCart size={20} />
                        <Typography variant='body1' sx={{ paddingX: 1 }}>
                            Cart
                        </Typography>
                    </Button>
                </Toolbar>
            </Container>
        </AppBar>
    )
}

export default NavBar