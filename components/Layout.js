import Head from 'next/head'
import React from 'react'
import NavBar from './NavBar'

const Layout = ({ children }) => {

    return (
        <>
            <Head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Home</title>
            </Head>
            <NavBar/>
            <main>{children}</main>
        </>
    )
}

export default Layout