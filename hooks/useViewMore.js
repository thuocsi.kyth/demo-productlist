import { Typography } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'

const DescriptionComponent = ({ children }) => {
    return (
        <Typography variant="body1" sx={{ fontSize: 18, lineHeight: 1.4 }}>
            {children}
        </Typography>
    )
}

const useViewMore = (limit = 200) => {

    const Description = ({ content }) => {
        const temp = useRef()
        const [description, setDescription] = useState('')
        const [isHide, setIsHide] = useState(true)

        useEffect(() => {
            temp.current = content
            setDescription(content?.slice(0, limit))
        }, [content])
        
        const handleOnClick = () => {
            setIsHide(!isHide)
            if (isHide) {
                setDescription(temp.current)
            } else {
                setDescription(temp.current.slice(0, limit))
            }
        }
        return (
            <DescriptionComponent>
                {description}
                {(temp.current?.length <= limit) ? '' : <Typography variant="span" onClick={handleOnClick} sx={{ paddingLeft: 1, color: 'blue', cursor: 'pointer' }}>{isHide ? 'read more...' : 'hide'}</Typography>}
            </DescriptionComponent>
        )
    }
    return {
        Description
    }
}

export default useViewMore